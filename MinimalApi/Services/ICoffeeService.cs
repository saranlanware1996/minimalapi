﻿using MinimalApi.Models;

namespace MinimalApi.Services
{
    public interface ICoffeeService
    {

        public CoffeeModel Create(CoffeeModel coffee);
        public CoffeeModel? Get(int Id);
        public List<CoffeeModel> List();
        public CoffeeModel Update(CoffeeModel coffee);
        public bool Delete(int Id);

    }
}
