﻿using MinimalApi.Models;
using MinimalApi.Repositories;

namespace MinimalApi.Services
{
    public class CoffeService : ICoffeeService
    {
        public CoffeeModel Create(CoffeeModel coffee)
        {
            coffee.Id = CoffeeRepository.Coffees.Count + 1;
            CoffeeRepository.Coffees.Add(coffee);
            return coffee;

        }

        public bool Delete(int Id)
        {
           var result =  CoffeeRepository.Coffees.FirstOrDefault(c => c.Id == Id);
            if (result is null) return false;
            CoffeeRepository.Coffees.Remove(result);
            return true;
        }

        public CoffeeModel? Get(int Id)
        {
           var listCoffee =  CoffeeRepository.Coffees.Select(k=>k.Id == Id).ToList();
            var listCoffee2 = CoffeeRepository.Coffees.FirstOrDefault(k => k.Id == Id);
            if (listCoffee is null) return null;
            return listCoffee2;

        }

        public List<CoffeeModel> List()
        {
            throw new NotImplementedException();
        }

        public CoffeeModel Update(CoffeeModel coffee)
        {
           var update = CoffeeRepository.Coffees.FirstOrDefault(c => c.Id == coffee.Id);
            if (update is null) return null;
            update.Tittle = coffee.Tittle;
            update.Description = coffee.Description;
            return coffee;


        }
    }
}
