using MinimalApi.Models;
using MinimalApi.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<ICoffeeService, CoffeService>();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();


app.MapPost("/create",
    (CoffeeModel coffe, ICoffeeService service) =>
    {
        if (coffe.Description == null)
        {
            return null;

        }
        var result = service.Create(coffe);
        return Results.Ok(result);
    });

app.MapDelete("/delete",
    (int Id, ICoffeeService service) =>
    {
        
        var result = service.Delete(Id);
        return Results.Ok(result);
    });

app.MapPut("/update",
   (CoffeeModel coffe, ICoffeeService service) =>
   {

        var result = service.Update(coffe);
        return Results.Ok(result);
    });

app.MapGet("/get",
   (int id, ICoffeeService service) =>
   {

       var result = service.Get(id);
       return Results.Ok(result);
   });

app.Run();

